enum
{
    T_IDENTIFIER = 256,
    T_INTEGER,
    T_CHARACTER,
    T_STRING,
    K_TYPEDEF,
    K_VOID,
    K_CHAR,
    K_INT,
    K_STRUCT,
    K_UNION,
    K_ENUM,
    K_IF,
    K_ELSE,
    K_WHILE,
    K_DO,
    K_FOR,
    K_CONTINUE,
    K_BREAK,
    K_RETURN,
    K_SIZEOF,
    MUL_ASSIGN,
    DIV_ASSIGN,
    MOD_ASSIGN,
    ADD_ASSIGN,
    SUB_ASSIGN,
    SHL_ASSIGN,
    SHR_ASSIGN,
    AND_ASSIGN,
    XOR_ASSIGN,
    OR_ASSIGN,
    OR,
    AND,
    EQ,
    NE,
    LE,
    GE,
    SHL,
    SHR,
    INC,
    DEC,
    PTR
};

struct Token
{
    int kind;
    union {
        int n;
        char *s;
    };
};

struct Token* mkToken(int kind)
{
    struct Token *t = malloc(sizeof(struct Token));
    t->kind = kind;
    return t;
}

int nextToken()
{
    for (;;)
    {
        while (isspace(lPeek())) lSkip();
    
        if (lMatch('/'))
        {
            if (lMatch('/'))
            {
                while (!lMatch('\n')) lSkip();
                continue;
            }
            if (lMatch('*'))
            {
                for (;;)
                {
                    while (!lMatch('*')) lSkip();
                    if (lMatch('/')) continue;
                }
            }
            if (lMatch('=')) return mkToken(DIV_ASSIGN);
            return mkToken('/');
        }
        if (lMatch('(')) return mkToken('(');
        if (lMatch(')')) return mkToken(')');
        if (lMatch(';')) return mkToken(')');
        if (lMatch(',')) return mkToken(',');
        if (lMatch('='))
        {
            if (lMatch('=')) return mkToken(EQ);
            return mkToken('=');
        }
        if (lMatch('{')) return mkToken('{');
        if (lMatch('}')) return mkToken('}');
        if (lMatch('[')) return mkToken('[');
        if (lMatch(']')) return mkToken(']');
        if (lMatch('*'))
        {
            if (lMAtch('=')) return mkToken(MUL_ASSIGN);
            return mkToken('*');
        }
        if (lMatch('|'))
        {
            if (lMatch('=')) return mkToken(OR_ASSIGN);
            if (lMatch('|')) return mkToken(OR);
            return mkToken('|');
        }
        if (lMatch('^'))
        {
            if (lMatch('=')) return mkToken(XOR_ASSIGN);
            return mkToken('^');
        }
        if (lMatch('&'))
        {
            if (lMatch('=')) return mkToken(AND_ASSIGN);
            if (lMatch('&')) return mkToken(AND);
            return mkToken('&');
        }
        if (lMatch('<'))
        {
            if (lMatch('=')) return mkToken(LE);
            if (lMatch('<'))
            {
                if (lMatch('=')) return mkToken(SHL_ASSIGN);
                return mkToken(SHL);
            }
            return mkToken('<');
        }
        if (lMatch('>'))
        {
            if (lMatch('=')) return mkToken(GE);
            if (lMatch('>'))
            {
                if (lMatch('=')) return mkToken(SHR_ASSIGN);
                return mkToken(SHR);
            }
            return mkToken('>');
        }
        if (lMatch('+'))
        {
            if (lMatch('=')) return mkToken(ADD_ASSIGN);
            if (lMatch('+')) return mkToken(INC);
            return mkToken('+');
        }
        if (lMatch('-'))
        {
            if (lMatch('=')) return mkToken(SUB_ASSIGN);
            if (lMatch('-')) return mkToken(DEC);
            if (lMatch('>')) return mkToken(PTR);
            return mkToken('-');
        }
        if (lMatch('*'))
        {
            if (lMatch('=')) return mkToken(MUL_ASSIGN);
            return mkToken('*');
        }
        if (lMatch('%'))
        {
            if (lMatch('=')) return mkToken(MOD_ASSIGN);
            return mkToken('%');
        }
        if (lMatch('~')) return mkToken('~');
        if (lMatch('!'))
        {
            if (lMatch('=')) return mkToken(NE);
            return mkToken('!');
        }
        if (lMatch('.')) return mkToken('.');
    
        if (isalpha(lPeek()))
        {
            char id[1024];
            int i = 0;
            while (isalnum(lPeek()))
            {
                id[i++] = lPeek();
                lSkip();
            }
            id[i] = '\0';
            
            if (strcmp("typedef", id) == 0) return mkToken(K_TYPEDEF);
            if (strcmp("void", id) == 0) return mkToken(K_VOID);
            if (strcmp("char", id) == 0) return mkToken(K_CHAR);
            if (strcmp("int", id) == 0) return mkToken(K_INT);
            if (strcmp("struct", id) == 0) return mkToken(K_STRUCT);
            if (strcmp("union", id) == 0) return mkToken(K_UNION);
            if (strcmp("enum", id) == 0) return mkToken(K_ENUM);
            if (strcmp("if", id) == 0) return mkToken(K_IF);
            if (strcmp("else", id) == 0) return mkToken(K_ELSE);
            if (strcmp("while", id) == 0) return mkToken(K_WHILE);
            if (strcmp("do", id) == 0) return mkToken(K_DO);
            if (strcmp("for", id) == 0) return mkToken(K_FOR);
            if (strcmp("continue", id) == 0) return mkToken(K_CONTINUE);
            if (strcmp("break", id) == 0) return mkToken(K_BREAK);
            if (strcmp("return", id) == 0) return mkToken(K_RETURN);
            if (strcmp("sizeof", id) == 0) return mkToken(K_SIZEOF);
            
            /* ignore unsupported keywords in ANSI C */
            if (strcmp("auto", id) == 0) continue;
            if (strcmp("case", id) == 0) continue;
            if (strcmp("const", id) == 0) continue;
            if (strcmp("default", id) == 0) continue;
            if (strcmp("double", id) == 0) continue;
            if (strcmp("extern", id) == 0) continue;
            if (strcmp("float", id) == 0) continue;
            if (strcmp("goto", id) == 0) continue;
            if (strcmp("long", id) == 0) continue;
            if (strcmp("register", id) == 0) continue;
            if (strcmp("short", id) == 0) continue;
            if (strcmp("signed", id) == 0) continue;
            if (strcmp("static", id) == 0) continue;
            if (strcmp("switch", id) == 0) continue;
            if (strcmp("unsigned", id) == 0) continue;
            if (strcmp("volatile", id) == 0) continue;
            
            {
                struct Token *t = mkToken(T_IDENTIFIER);
                t->s = malloc(strlen(id) + 1);
                strcpy(t->s, id);
                return t;
            }
        }
    
        if (isdigit(lPeek()))
        {
            struct Token *t = mkToken(T_INTEGER);
            t->n = 0;
            while (isdigit(lPeek()))
            {
                t->n *= 10;
                t->n += lPeek() - '0';
                lSkip();
            }
            return t;
        }
    
        if (lMatch('\''))
        {
            struct Token *t = mkToken(T_CHARACTER);
            if (lMatch('\\')) t->n = escapeChar(lPeek());
            else t->n = lPeek();
            lSkip();
            if (lMatch('\'')) return t;
            return NULL;
        }
    
        if (lMatch('"'))
        {
            char str[1024];
            int i = 0;
            for (;;)
            {
                if (lMatch('"'))
                {
                    struct Token *t = mkToken(T_STRING);
                    t->s = malloc(strlen(str) + 1);
                    strcpy(t->s, str);
                    return t;
                }
                if (lMatch('\\')) str[i++] = escapeChar(lPeek());
                else str[i++] = lPeek();
                lSkip();
            }
        }
    
        return NULL;
    }
}
