enum
{
    T_IDENTIFIER = 256,
    T_INTEGER,
    T_CHARACTER,
    T_STRING,
    K_TYPEDEF,
    /* TODO define other token kinds */
};

struct Token
{
    int kind;
    union {
        int n;
        char *s;
    };
};

struct Token* mkToken(int kind)
{
    struct Token *t = malloc(sizeof(struct Token));
    t->kind = kind;
    return t;
}

int nextToken()
{
    for (;;)
    {
        while (isspace(lPeek())) lSkip();
    
        if (lMatch('/'))
        {
            if (lMatch('/'))
            {
                while (!lMatch('\n')) lSkip();
                continue;
            }
            if (lMatch('*'))
            {
                /* TODO handle multi-line comment */
            }
            if (lMatch('=')) return mkToken(DIV_ASSIGN);
            return mkToken('/');
        }
        /* TODO handle other operators */
    
        if (isalpha(lPeek()))
        {
            char id[1024];
            int i = 0;
            while (isalnum(lPeek()))
            {
                id[i++] = lPeek();
                lSkip();
            }
            id[i] = '\0';
            
            if (strcmp("typedef", id) == 0) return mkToken(K_TYPEDEF);
            /* TODO handle other keywords */
            
            /* TODO ignore unsupported keywords in ANSI C */
            
            {
                struct Token *t = mkToken(T_IDENTIFIER);
                t->s = malloc(strlen(id) + 1);
                strcpy(t->s, id);
                return t;
            }
        }
    
        if (isdigit(lPeek()))
        {
            /* TODO handle integer constant */
        }
    
        if (lMatch('\''))
        {
            struct Token *t = mkToken(T_CHARACTER);
            if (lMatch('\\')) t->n = escapeChar(lPeek());
            else t->n = lPeek();
            lSkip();
            if (lMatch('\'')) return t;
            return NULL;
        }
    
        if (lMatch('"'))
        {
            /* TODO handle string constant */
        }
    
        return NULL;
    }
}
