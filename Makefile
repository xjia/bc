verbatim: verbatim.l
	lex verbatim.l
	cc lex.yy.c -ll -o verbatim
	rm lex.yy.c

clean:
	rm -f verbatim
